"use strict";

// 1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.

// let num1 = 40;
// let num2 = 5;
// function divisionNumber(a, b) {
//   return a / b;
// }
// console.log(divisionNumber(num1, num2));

// 2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.
// - Отримати за допомогою модального вікна браузера два числа. Провалідувати отримані значення(перевірити, що отримано числа). Якщо користувач ввів не число, запитувати до тих пір, поки не введе число
// - Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /. Провалідувати отримане значення. Якщо користувач ввів не передбачене значення, вивести alert('Такої операції не існує').
// - Створити функцію, в яку передати два значення та операцію.
// - Вивести у консоль результат виконання функції.

// let userNumber1 = prompt(`First number`);
// if (userNumber1 !== null && userNumber1 !== "" && !isNaN(userNumber1)) {
//   console.log(`Введене число ${userNumber1}`);
// } else {
//   while (userNumber1) {
//     let userNumber1 = prompt(`Введено не число! Будьласка, спробуйте ще раз`);
//     if (userNumber1 !== null && userNumber1 !== "" && !isNaN(userNumber1)) {
//       console.log(`Ваше перше число ${userNumber1}`);
//       break;
//     }
//   }
// }
// let userNumber2 = prompt(`Second number`);
// if (userNumber2 !== null && userNumber2 !== "" && !isNaN(userNumber2)) {
//   console.log(`Введене число ${userNumber2}`);
// } else {
//   while (userNumber2) {
//     let userNumber2 = prompt(`Введено не число! Будьласка, спробуйте ще раз`);
//     if (userNumber2 !== null && userNumber2 !== "" && !isNaN(userNumber2)) {
//       console.log(`Ваше перше число ${userNumber2}`);
//       break;
//     }
//   }
// }
// let performing = prompt(`What math operation do you want to do?`);
// if (
//   performing == "+" ||
//   performing == "-" ||
//   performing == "/" ||
//   performing == "*"
// ) {
// } else {
//   alert(`Такої операції не існує.`);
//     let performing = prompt(`What math operation do you want to do?`);
//     if (
//       performing == "+" ||
//       performing == "-" ||
//       performing == "/" ||
//       performing == "*"
//     );
//  }
// function performingMathOperations(a, b, performing) {
//   switch (performing) {
//     case `+`:
//       return Number(a) + Number(b);
//     case `-`:
//       return a - b;
//     case `*`:
//       return a * b;
//     case `/`:
//       if (userNumber2 == `0`) {
//         alert(`На ноль ділити не можна`);
//       } else {
//         return a / b;
//       }
//   }
// }
// console.log(performingMathOperations(userNumber1, userNumber2, performing));
// 3. Опціонально. Завдання:
// Реалізувати функцію підрахунку факторіалу числа.
// Технічні вимоги:
// - Отримати за допомогою модального вікна браузера число, яке введе користувач.
// - За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.
// - Використовувати синтаксис ES6 для роботи зі змінними та функціями.

// let userNumber = prompt(`Enter your number`);
// if (userNumber !== null && userNumber !== "" && !isNaN(userNumber)) {
//   console.log(`Your number is ${userNumber}`);
// }
// function getFactorial(userNumber) {
//   let result = 1;
//   for (let i = 1; i <= userNumber; ++i){
//     result = result * i;
// }
// return result;
// }
// console.log(getFactorial(userNumber));
